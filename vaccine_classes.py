# PATIENT CLASS
class patient():

  def __init__(self):
    # object properties
    self.pId = int
    self.pFname = ""
    self.pLname = ""
    self.hasVacA = False
    self.hasVacB = False
    self.hasVacC = False
    self.hasSymptA = False
    self.hasSymptB = False
    self.hasSymptC = False
  # object methods
  def report(self):  # reports vaccination data for individual
    print()
    print("First Name: ", self.pFname)
    print("Last Name: ", self.pLname)
    print("Vaccine Status: ")
    print("Vac_A Status: ", self.hasVacA)
    print("Vac_B Status: ", self.hasVacB)
    print("Vac_C Status: ", self.hasVacC)
    print("Symptom Status: ")
    print("Sympt_A Status: ", self.hasSymptA)
    print("Sympt_B Status: ", self.hasSymptB)
    print("Sympt_C Status: ", self.hasSymptC)
    

  # Toggle Vaccines
  def vac_AToggle(self):
    self.hasVacA = not self.hasVacA

  def vac_BToggle(self):
    self.hasVacB = not self.hasVacB

  def vac_CToggle(self):
    self.hasVacC = not self.hasVacC
  
  # Toggle Symptoms
  def sympt_AToggle(self):
    self.hasSymptA = not self.hasSymptA
  
  def sympt_BToggle(self):
    self.hasSymptB = not self.hasSymptB
    
  def sympt_CToggle(self):
    self.hasSymptC = not self.hasSymptC
  
  # Set Names
  def setFirstName(self, name):
    self.pFname = name
  
  def setLastName(self, name):
    self.pLname = name

# PATIENT LIST CLASS
class patient_list():

  def __init__(self):
    self.patients = []

  def add_patient(self, first_names, last_names, toggle_states):
        for i in range(len(first_names)):
            x = patient()
            x.setFirstName(first_names[i])
            x.setLastName(last_names[i])

            # Update vaccination and symptom status based on toggle states
            for j in range(3):
                vaccine_status = toggle_states[i * 6 + j]
                symptom_status = toggle_states[i * 6 + j + 3]
                if j == 0:
                    x.hasVacA = vaccine_status
                    x.hasSymptA = symptom_status
                elif j == 1:
                    x.hasVacB = vaccine_status
                    x.hasSymptB = symptom_status
                elif j == 2:
                    x.hasVacC = vaccine_status
                    x.hasSymptC = symptom_status

            self.patients.append(x)
      
  def request_report(self):
        print("Request vaccination data: ")
        pNumInput = get_integer_input(
            "Which patient number would you like to view the vaccination status of? (1-15) ",
            1,
            len(self.patients)
        )
        self.patients[pNumInput - 1].report()

  def report_totals(self):  #Report total vaccination status
    vacACount = 0
    vacBCount = 0
    vacCCount = 0
    for i in range(len(self.patients)):
      if self.patients[i].hasVacA == True:
        vacACount += 1
      if self.patients[i].hasVacB == True:
        vacBCount += 1
      if self.patients[i].hasVacC == True:
        vacCCount += 1
    return(
      f"vac_A total: {vacACount}, vac_B total: {vacBCount}, vac_C total: {vacCCount}, "
    )
    
  def report_sympt_totals(self): # report 
        symptA_VacA_Count = 0
        symptB_VacA_Count = 0
        symptC_VacA_Count = 0
        symptA_VacB_Count = 0
        symptB_VacB_Count = 0
        symptC_VacB_Count = 0
        symptA_VacC_Count = 0
        symptB_VacC_Count = 0
        symptC_VacC_Count = 0

        for patient in self.patients:
            if patient.hasVacA:
                if patient.hasSymptA:
                    symptA_VacA_Count += 1
                if patient.hasSymptB:
                    symptB_VacA_Count += 1
                if patient.hasSymptC:
                    symptC_VacA_Count += 1

            if patient.hasVacB:
                if patient.hasSymptA:
                    symptA_VacB_Count += 1
                if patient.hasSymptB:
                    symptB_VacB_Count += 1
                if patient.hasSymptC:
                    symptC_VacB_Count += 1

            if patient.hasVacC:
                if patient.hasSymptA:
                    symptA_VacC_Count += 1
                if patient.hasSymptB:
                    symptB_VacC_Count += 1
                if patient.hasSymptC:
                    symptC_VacC_Count += 1

        return(f"VacA Total sympt: A = {symptA_VacA_Count} B = {symptB_VacA_Count} C = {symptC_VacA_Count}... VacB Total sympt: A = {symptA_VacB_Count} B = {symptB_VacB_Count} C = {symptC_VacB_Count}... VacC Total sympt: A = {symptA_VacC_Count} B = {symptB_VacC_Count} C = {symptC_VacC_Count}")

    
  def reset_patients(self): # reset all patients' status to false
    for patient in self.patients:
        patient.hasVacA = False
        patient.hasVacB = False
        patient.hasVacC = False
        patient.hasSymptA = False
        patient.hasSymptB = False
        patient.hasSymptC = False
        
  def clear_patient_list(self):
      self.patients = []  # Clear the list of patients

# Create global patient_list
patientList = patient_list()