from vaccine_classes import *
import pygame

# Create Display window
WINDOW_HEIGHT = 500
WINDOW_WIDTH = 1400
pygame.init()
window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption('GUI DEMO')
timer = pygame.time.Clock() #load clock
fps = 60 #set fps
font = pygame.font.Font('freesansbold.ttf', 18) #load font

# Text Box Attributes
user_text = ""
input_rect = pygame.Rect(200, 200, 140, 32)
color_active = pygame.Color('lightskyblue3') # active text color
color_passive = pygame.Color('gray15') # passive text color
color = color_passive # passive is on by default

active = False

# Game Variables
menu_state ="main" #This will allow state management of menus


# Display Text Class
class DisplayText:
    def __init__(self, text, x, y, font_size=18, color='black'):
        self.text = text
        self.x = x
        self.y = y
        self.font_size = font_size
        self.color = pygame.Color('red')
        self.font = pygame.font.Font('freesansbold.ttf', font_size)
        self.surface = self.font.render(self.text, True, self.color)

    def update_text(self, new_text):
        self.text = new_text
        self.surface = self.font.render(self.text, True, self.color)

    def draw(self, window):
        window.blit(self.surface, (self.x, self.y))

# User Input Box Class
class TextBox:
    def __init__(self, x, y, width, height, font_size=18, text_color='black', active_color='lightskyblue3',
                 passive_color='gray15'):
        self.rect = pygame.Rect(x, y, width, height)
        self.text = ''
        self.font_size = font_size
        self.text_color = pygame.Color(text_color)
        self.active_color = pygame.Color(active_color)
        self.passive_color = pygame.Color(passive_color)
        self.color = self.passive_color
        self.active = False

    def draw(self):
        pygame.draw.rect(window, self.color, self.rect, 2)
        text_surface = font.render(self.text, True, self.text_color)
        window.blit(text_surface, (self.rect.x + 5, self.rect.y + 5))

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.active = not self.active
            else:
                self.active = False
            self.color = self.active_color if self.active else self.passive_color

        elif event.type == pygame.KEYDOWN and self.active:
            if event.key == pygame.K_BACKSPACE:
                self.text = self.text[:-1]
            else:
                self.text += event.unicode

# Button Class
class Button:
    def __init__(self, text, x_pos, y_pos, cooldown_ms=500):
        self.text = text
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.state = 'idle'
        self.enabled = True
        self.cooldown_ms = cooldown_ms  # Cooldown time in milliseconds
        self.last_click_time = 0
        self.draw()

    def draw(self):
        button_text = font.render(self.text, True, 'black')
        button_rect = pygame.Rect((self.x_pos, self.y_pos), (110, 25))
        
        if self.enabled:
            if self.check_click():
                self.state = 'armed'
            else:
                self.state = 'idle'
            pygame.draw.rect(window, 'darkgray' if self.state == 'armed' else 'lightgray', button_rect, 0)
            pygame.draw.rect(window, 'black', button_rect, 2)
            window.blit(button_text, (self.x_pos + 3, self.y_pos + 3))
        else:
            self.state = 'idle'
            pygame.draw.rect(window, 'black', button_rect, 0)

    def check_click(self):
        current_time = pygame.time.get_ticks()

        # Check if enough time has passed since the last click
        if current_time - self.last_click_time > self.cooldown_ms:
            mouse_pos = pygame.mouse.get_pos()
            left_click = pygame.mouse.get_pressed()[0]
            button_rect = pygame.Rect((self.x_pos, self.y_pos), (100, 25))
            
            if left_click and button_rect.collidepoint(mouse_pos) and self.enabled:
                # Update the last click time
                self.last_click_time = current_time
                return True

        return False

class ToggleButton:
    def __init__(self, x, y, radius, default_value=False):
        self.x = x
        self.y = y
        self.radius = radius
        self.value = default_value
        self.color = (0, 255, 0) if default_value else (255, 0, 0)  # Green if True, Red if False

    def draw(self, window):
        pygame.draw.circle(window, self.color, (self.x, self.y), self.radius)

    def check_click(self, mouse_pos, click_event):
        distance_squared = (mouse_pos[0] - self.x) ** 2 + (mouse_pos[1] - self.y) ** 2
        if distance_squared <= self.radius ** 2:
            if click_event:
                self.toggle()

    def toggle(self):
        self.value = not self.value
        self.color = (0, 255, 0) if self.value else (255, 0, 0)  # Green if True, Red if False

# Patient Info Display Class
class PatientInfoDisplay:
    def __init__(self, x, y, patient):
        self.x = x
        self.y = y
        self.patient = patient

    def draw(self, window):
        # Display first name
        text_surface = font.render(self.patient.pFname, True, 'black')
        window.blit(text_surface, (self.x, self.y))

        # Display last name
        text_surface = font.render(self.patient.pLname, True, 'black')
        window.blit(text_surface, (self.x + 150, self.y))

        # Display vaccine statuses
        vax_a_status = "True" if self.patient.hasVacA else "False"
        text_surface = font.render(vax_a_status, True, 'black')
        window.blit(text_surface, (self.x + 300, self.y))

        vax_b_status = "True" if self.patient.hasVacB else "False"
        text_surface = font.render(vax_b_status, True, 'black')
        window.blit(text_surface, (self.x + 400, self.y))

        vax_c_status = "True" if self.patient.hasVacC else "False"
        text_surface = font.render(vax_c_status, True, 'black')
        window.blit(text_surface, (self.x + 500, self.y))

        # Display symptom statuses
        sympt_a_status = "True" if self.patient.hasSymptA else "False"
        text_surface = font.render(sympt_a_status, True, 'black')
        window.blit(text_surface, (self.x + 600, self.y))

        sympt_b_status = "True" if self.patient.hasSymptB else "False"
        text_surface = font.render(sympt_b_status, True, 'black')
        window.blit(text_surface, (self.x + 700, self.y))

        sympt_c_status = "True" if self.patient.hasSymptC else "False"
        text_surface = font.render(sympt_c_status, True, 'black')
        window.blit(text_surface, (self.x + 800, self.y))
        
# BUTTONS
# main menu buttons
input_button = Button("Input Data", 140, 420) # opens menu for input patients
rep_patient_button = Button("Patient Info", 270, 420) # view a specific patient
rep_vaccs_button = Button("Rep Vaccs", 400, 420) # report vaccination totals among all patients
rep_sympts_button = Button("Rep Sympts", 530, 420) # report symtom totals for each vaccine type
exit_button = Button("Exit", 1250, 10) # exits GUI

# input menu buttons
back_button = Button("Back", 10, 10) # returns to main menu
submit_button = Button("Submit", 650, 420) # submits patient data
reset_button = Button("Reset Data", 450, 420) # resets all patient data

# patient info menu buttons
prev_button = Button("Prev.", 450, 420) # previous patient
next_button = Button("Next", 650, 420) # next patient
search_button = Button("Search P#", 1070, 420)

# Records Input Boxes
first_name_box = TextBox(40, 75, 140, 32)
last_name_box = TextBox(235, 75, 140, 32)

# Patient info Input Boxes
search_input_box = TextBox(1000, 420, 50, 32)
    
# Display Texts

# Input Data Display Text
display_first_name = DisplayText("First Name", 50, 50)
display_last_name = DisplayText("Last Name", 245, 50)
patient_1_text = DisplayText("0.", 10, 80)
Vax_A = DisplayText("Vax_A", 400, 50)
Vax_B = DisplayText("Vax_B", 500, 50)
Vax_C = DisplayText("Vax_C", 600, 50)
Sympt_A = DisplayText("Sympt_A", 700, 50)
Sympt_B = DisplayText("Sympt_B", 800, 50)
Sympt_C = DisplayText("Sympt_C", 900, 50)
display_first_name_2 = DisplayText("First Name", 75, 50)

labels_to_draw = [display_first_name, display_last_name, patient_1_text, Vax_A, Vax_B, Vax_C, Sympt_A, Sympt_B, Sympt_C]
p_info_labels_to_draw = [display_first_name_2, display_last_name, Vax_A, Vax_B, Vax_C, Sympt_A, Sympt_B, Sympt_C]

# Patient Data Display Text

# Main Display Text
vacc_totals_text = DisplayText("", 400, 200)
sympt_totals_text = DisplayText("", 300, 100)

# TOGGLE BUTTONS START
# Toggle Buttons for Patient
vax_a_button_p1 = ToggleButton(Vax_A.x + 30, patient_1_text.y + 5, 15)
vax_b_button_p1 = ToggleButton(Vax_B.x + 30, patient_1_text.y + 5, 15)
vax_c_button_p1 = ToggleButton(Vax_C.x + 30, patient_1_text.y + 5, 15)
symp_a_button_p1 = ToggleButton(Sympt_A.x + 30, patient_1_text.y + 5, 15)
symp_b_button_p1 = ToggleButton(Sympt_B.x + 30, patient_1_text.y + 5, 15)
symp_c_button_p1 = ToggleButton(Sympt_C.x + 30, patient_1_text.y + 5, 15)


toggle_buttons = [
    vax_a_button_p1, vax_b_button_p1, vax_c_button_p1, symp_a_button_p1, symp_b_button_p1, symp_c_button_p1
]

# TOGGLE BUTTONS END

# Other external vars
patient_info_display_p1 = None

# Game Loop
run = True

while run:
    window.fill((202, 228, 241))
    timer.tick(fps)
        
    
    # Main Menu
    if menu_state == "main":
        input_button.draw()
        rep_patient_button.draw()
        rep_vaccs_button.draw()
        rep_sympts_button.draw()
        vacc_totals_text.draw(window)
        sympt_totals_text.draw(window)
        
        # Main Event Handlers
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if input_button.check_click(): #check if button click
                menu_state = "records"
            if rep_patient_button.check_click():
                menu_state = "patient_info"
            if exit_button.check_click():
                run = False
            if rep_vaccs_button.check_click():
                patientList.report_totals()
                vacc_totals_text.update_text(patientList.report_totals())
                pygame.display.update()
            if rep_sympts_button.check_click():
                patientList.report_sympt_totals()
                sympt_totals_text.update_text(patientList.report_sympt_totals())
                pygame.display.update()
                
    # Records Menu
    if menu_state == "records":
        if active:
            color = color_active
        else:
            color = color_passive
        back_button.draw()
        submit_button.draw()
        reset_button.draw()
        
        # Draw User Input Boxes
        first_name_box.draw()
        last_name_box.draw()
        
        # Draw Toggle Buttons
        for toggle_button in toggle_buttons:
            toggle_button.draw(window)
        
        # Draw Labels
        for label in labels_to_draw:
            label.draw(window)
        
        
        # Records Event Handlers
        for event in pygame.event.get():
            if back_button.check_click():
                menu_state = "main"
            if exit_button.check_click():
                run = False
            first_name_box.handle_event(event)
            last_name_box.handle_event(event)
            
            # SUBMIT BUTTON HANDLERS
            if submit_button.check_click():
    # Prepare data for add_patient method
                first_name = first_name_box.text
                last_name = last_name_box.text
                toggle_states = [toggle_button.value for toggle_button in toggle_buttons]
                patient_1_text.update_text(str(len(patientList.patients) + 1) + ".")

                # Use the modified add_patient method
                patientList.add_patient([first_name], [last_name], toggle_states)
                print("Patient data submitted successfully.")

                # Clear input text boxes for the next entry
                first_name_box.text = ""
                last_name_box.text = ""

                # Toggle off (set to red) all toggle buttons for the next entry
                for toggle_button in toggle_buttons:
                    toggle_button.value = False
                    toggle_button.color = (255, 0, 0)  # Red
            
            # Reset Button Hanbdlers
            if reset_button.check_click():
                
                # Toggle off (set to red) all toggle buttons
                for toggle_button in toggle_buttons:
                    toggle_button.value = False
                    toggle_button.color = (255, 0, 0)  # Red

                # Clear patient list
                patientList.reset_patients()
                patientList.clear_patient_list()
                patient_1_text.update_text("0.")
                print("Data reset successfully.")
                
            
            # ToggleButton Events
            for toggle_button in toggle_buttons:
                toggle_button.check_click(pygame.mouse.get_pos(), event.type == pygame.MOUSEBUTTONDOWN)
    
    # Patient Info Menu
    if menu_state == "patient_info":
        back_button.draw()
        prev_button.draw()
        next_button.draw()
        search_button.draw()
        search_input_box.draw()
        # Check if the patient list is not empty
        if patientList.patients:
            if patient_info_display_p1 is None:
                patient_info_display_p1 = PatientInfoDisplay(100, 100, patientList.patients[0])
            if not hasattr(patient_info_display_p1, 'index'):
                # Initialize the index if it doesn't exist
                patient_info_display_p1.index = 0

            # Draw patient info at the center of the window
            if patient_info_display_p1 == None:
                pass
            else:
                patient_info_display_p1.draw(window)
            
            for label in p_info_labels_to_draw:
                label.draw(window)
    
        # Patient Info Event Handlers
        for event in pygame.event.get():
            if back_button.check_click():
                menu_state = "main"
            if exit_button.check_click():
                run = False
            if search_button.check_click():
                try:
                    # Get the entered index from the TextBox and update PatientInfoDisplay
                    entered_index = int(search_input_box.text)
                    if 0 <= entered_index < len(patientList.patients):
                        patient_info_display_p1.index = entered_index
                        patient_info_display_p1.patient = patientList.patients[entered_index]
                        pygame.display.update()
                    else:
                        print("Invalid index. Please enter a valid index.")
                except ValueError:
                    print("Invalid input. Please enter a valid index.")

            # TextBox Event Handler
            search_input_box.handle_event(event)
            if patientList.patients:
                if prev_button.check_click():
                        patient_info_display_p1.index = (patient_info_display_p1.index - 1) % len(patientList.patients)
                        patient_info_display_p1.patient = patientList.patients[patient_info_display_p1.index]
                        pygame.display.update()
                        
                if next_button.check_click():
                        patient_info_display_p1.index = (patient_info_display_p1.index + 1) % len(patientList.patients)
                        patient_info_display_p1.patient = patientList.patients[patient_info_display_p1.index]
                        pygame.display.update()
                        
            # Error handle if list is None
            if patient_info_display_p1 == None:
                pass
            else:
                patient_info_display_p1.draw(window)

        
    exit_button.draw() # Stand-Alone exit button
    # Draw window elements
    pygame.display.update()

pygame.quit()